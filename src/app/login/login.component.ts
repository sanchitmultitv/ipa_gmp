import { Component, OnInit, HostListener } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

import { Router } from '@angular/router';
import { FetchDataService } from '../services/fetch-data.service';
import { AuthService } from '../services/auth.service';
function emailDomainValidator(control: FormControl) {
  let email = control.value;
  if (email && email.indexOf("@") != -1) {
    let [_, domain] = email.split("@");
    if (domain !== "sanofi.com" && domain !== "sanofi-india.com") {
      return {
        emailDomain: {
          parsedDomain: domain
        }
      }
    }
  }
  return null;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  invalidLogin;
  token;
  msg;
  loginForm: FormGroup
  coverImage = "../../assets/img/h-about.jpg";
  videoPlay = false;
  potrait = false;
  constructor(private router: Router, private _fd: FetchDataService, private auth: AuthService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    localStorage.setItem('user_guide', 'start');
    this.loginForm = this.formBuilder.group({
      // email: ['', [Validators.email, Validators.pattern("[^ @]*@[^ @]*"), emailDomainValidator]],
      name: ['', Validators.required],
      email: ['', Validators.required],
      // business_vertical: ['', Validators.required],
      // country: ['', Validators.required],

    });
    if (window.innerHeight > window.innerWidth) {
      this.potrait = true;
    } else {
      this.potrait = false;
    }


  }

  loggedIn() {
    const user = {
      name:  this.loginForm.get('name').value,
      // password: this.loginForm.get('password').value,
      event_id: 183,
      role_id: 1
    };
    var isMobile = {
      Android: function () {
        return navigator.userAgent.match(/Android/i);
      },
      BlackBerry: function () {
        return navigator.userAgent.match(/BlackBerry/i);
      },
      iOS: function () {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
      },
      Opera: function () {
        return navigator.userAgent.match(/Opera Mini/i);
      },
      Windows: function () {
        return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
      },
      any: function () {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
      }

    };

    // console.log("password",user.password)
    const formData=new FormData();
    // formData.append('name',this.loginForm.get('name').value);
    formData.append('email',this.loginForm.get('email').value);
    formData.append('company', this.loginForm.get('business_vertical').value);
    formData.append('country', this.loginForm.get('country').value);
    
    if(this.loginForm.valid){
      this.auth.loginMethod(formData).subscribe((res: any) => {
        if (res.code === 1) {
          if (isMobile.iOS()) {
            this.videoPlay = false;
            this.router.navigateByUrl('/lobby');
          }
          this.videoPlay = true;
          localStorage.setItem('virtual', JSON.stringify(res.result));
          this.videoPlay = true;
          let vid: any = document.getElementById('myVideo');
          vid.play();
          // this.router.navigateByUrl('/lobby');
  
          if (window.innerHeight > window.innerWidth) {
            this.potrait = true;
          } else {
            this.potrait = false;
          }
        } else {
          this.msg = 'Invalid Login';
          this.videoPlay = false;
          this.loginForm.reset();
        }
      }, (err: any) => {
        this.videoPlay = false;
        console.log('error', err)
      });
    }
  }
  @HostListener('window:resize', ['$event']) onResize(event) {
    if (window.innerHeight > window.innerWidth) {
      this.potrait = true;
    } else {
      this.potrait = false;
    }
  }

  endVideo() {
    // this.videoPlay = false;
    // this.potrait = false;
    this.router.navigateByUrl('/lobby');
    let welcomeAuido: any = document.getElementById('myAudio');
    welcomeAuido.play();
  }
  onLoginSubmit(){
    // let user = {
    //   event_id: 183,
    //   name: this.loginForm.value.name,
    //   email: this.loginForm.value.email
    // }
    if(this.loginForm.valid){
      this.auth.loginSubmit(this.loginForm.value.email).subscribe((res:any)=>{
        if(res.code == 1){
          localStorage.setItem('virtual', JSON.stringify(res.result));
          this.router.navigateByUrl('/animate');
        } else{
          this.invalidLogin = 'Invalid Login';
        }
      });
    }
  }
}
