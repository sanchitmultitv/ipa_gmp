import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import {WebcamModule} from 'ngx-webcam';
import {SocketIoConfig, SocketIoModule } from 'ngx-socket-io';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ThankyouComponent } from './thankyou/thankyou.component';
import { ThanksComponent } from './thanks/thanks.component';

const data: SocketIoConfig ={ url : 'https://belive.multitvsolution.com:8030', options: {} };


@NgModule({
  declarations: [
    AppComponent,
    ThankyouComponent,
    ThanksComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    WebcamModule,
    SocketIoModule.forRoot(data),
    NgbModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
