import { Router } from '@angular/router';
import { SteupServiceService } from 'src/app/services/steup-service.service';
import { MenuItems } from '../shared/menu.items';

import { Location } from '@angular/common';
import { Component, ElementRef, HostListener, OnInit, Renderer2, ViewChild } from '@angular/core';
import { FetchDataService } from 'src/app/services/fetch-data.service';
declare var $:any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @ViewChild('videoStream', { static: true }) videoElement: ElementRef;
  showImage = false;
  cameraStream;

  menuItems = MenuItems;
  constructor(private router: Router, private _analytics: SteupServiceService,private renderer: Renderer2, private _fd: FetchDataService, private location: Location) { }

  ngOnInit(): void {
  }
  exit(){
    localStorage.removeItem('virtual');
    this.router.navigate(['/login']);
  }
  stepup(action){
    this._analytics.stepUpAnalytics(action);
  }
  showPhotobooth(){
    // this.newImage = value
    // $('.window').modal('hide');
    $('#photoboothModal').modal('show');
    this.cameraStream = null;
    this.showImage = false;
    this.startWebcam();
  }
  startWebcam(){
    // const constraints = { "video": { width: 320, height: 180, facingMode: "user" }};
    let constraints = {
      facingMode: { exact: 'environment' },
      video: {
        width: { ideal: 640 },
        height: { ideal: 360 }
      }
    };
    navigator.mediaDevices.getUserMedia(constraints).then(this.gotMedia.bind(this)).catch(e=>{
        console.error('getusermedia() failed: '+e);
    })
  }
  theRecorder:any;
  gotMedia(stream){
    this.cameraStream = stream;
    this.renderer.setProperty(this.videoElement.nativeElement, 'srcObject', stream);
  }
  dataUrl;
  img;
  capturePhoto(){
    this.showImage = true;
    let canvasRec:any = document.getElementById('canvas');
    let preview:any = document.getElementById('videobgStream');
    let context = canvasRec.getContext('2d');
    var cw = 640;
    var ch = 360;
    canvasRec.width = cw;
    canvasRec.height = ch;
    context.drawImage(preview, 0, 0, cw, ch ); 
       
    let watermark = new Image();
    let bgimg = new Image();
    context.beginPath();
    
    bgimg.src = 'assets/ipf/images/photobooth/selfie.png';
    context.drawImage(bgimg, 0, 0, preview.videoWidth, preview.videoHeight);
    this.dataUrl = canvasRec.toDataURL('mime');
    this.img = canvasRec.toDataURL("image/png", 0.7);
  }
  @HostListener('document:click', ['$event', '$event.target'])
    onClick(event: MouseEvent, targetElement: HTMLElement): void {
      let selfie:any=document.getElementById('photoboothModal');
      if(targetElement===selfie){
        this.closePhotobooth();
      }
  }
  @HostListener('keydown', ['$event']) onKeyDown(e) {
    if (e.keyCode == 27) {
      this.closePhotobooth();
    }
  }
  closePhotobooth(){
    this.cameraStream.getTracks().forEach(track => { track.stop(); }); 
    $('#photoboothModal').modal('hide');
    // this.location.back();
  }
  closeWindow(){
    $('.window').modal('hide');
  }
  downloadPic(){
    window.location.href=this.img;
    let user_id = JSON.parse(localStorage.getItem('virtual'));
    let user_name = JSON.parse(localStorage.getItem('virtual'));
    const event_id = '183';
    const formData = new FormData();
    formData.append('user_id', user_id.id);
    formData.append('user_name', user_id.name);
    formData.append('image', this.img);
    formData.append('event_id', event_id);
    this._fd.uploadsample(formData).subscribe(res => {
      console.log('upload', res);
    });
    
  }
  reload() {
    this.cameraStream.getTracks().forEach(track => { track.stop(); }); 
    this.showImage = false;
    this.startWebcam();
  }
}
