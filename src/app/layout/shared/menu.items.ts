export const MenuItems = [
    {
        path: '/lobby',
        icon: 'assets/ipf/icons/new/1.png',
        name: 'lobby',
        analytics: 'click_lobby'
    },
    {
        datatarget: 'agendaModal',
        icon: 'assets/ipf/icons/new/2.png',
        name: 'agenda',
        analytics: 'click_agenda'
    },
    // {
    //     datatarget: 'speakersModal',
    //     icon: 'assets/ipf/icons/new/3.png',
    //     name: 'speaker profile',
    //     analytics: 'click_speakerprofile'
    // },
    {
        path: '/eventhall',
        icon: 'assets/ipf/icons/new/4.png',
        name: 'event hall',
        analytics: 'click_eventhall'
    },
    {
        function: 'showPhotobooth',
        icon: 'assets/ipf/icons/new/5.png',
        name: 'photobooth',
        analytics: 'click_photobooth'
    },
    {
        path: '/gratitudewall',
        icon: 'assets/ipf/icons/new/6.png',
        name: 'gratitude wall',
        analytics: 'click_gratitudewall'
    },
    // {
    //     path: '/lounge',
    //     icon: 'assets/ipf/icons/new/7.png',
    //     name: 'networking lounge',
    //     analytics: 'click_networkinglounge'
    // },
    {
        datatarget: 'helpDeskModal',
        icon: 'assets/ipf/icons/new/8.png',
        name: 'help desk',
        analytics: 'click_helpdesk'
    },
    {
        datatarget: 'feedbackModal',
        icon: 'assets/ipf/icons/new/9.png',
        name: 'feedback form',
        analytics: 'click_feedback'
    },
    {
        path: '/login',
        icon: 'assets/ipf/icons/new/10.png',
        name: 'exit',
        analytics: 'click_logout'
    },
];