import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PhotoboothComponent } from './components/photobooth/photobooth.component';
import { LayoutComponent } from './layout.component';


const routes: Routes = [
  {
    path:'',
    component: LayoutComponent,
    children:[
      { path: '', redirectTo: 'lobby', pathMatch: 'prefix' },
      {path: 'lobby', loadChildren: ()=> import('../@core/lobby/lobby.module').then(m => m.LobbyModule)},
      {path: 'lounge', loadChildren: ()=> import('../@core/networking-lounge/networking-lounge.module').then(m => m.NetworkingLoungeModule)},
      {path: 'eventhall', loadChildren: ()=> import('../@core/event-hall/event-hall.module').then(m => m.EventHallModule)},
      {path: 'gratitudewall', loadChildren: ()=> import('../@core/gratitude-wall/gratitude-wall.module').then(m => m.GratitudeWallModule)},
      {path: 'speakers', loadChildren: ()=> import('../@core/speaker-profile/speaker-profile.module').then(m => m.SpeakerProfileModule)},
      {path: 'products', loadChildren: ()=> import('../@core/products/products.module').then(m => m.ProductsModule)},
      {path: 'photobooth', component:PhotoboothComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
