import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GratitudeWallRoutingModule } from './gratitude-wall-routing.module';
import { GratitudeWallComponent } from './gratitude-wall.component';


@NgModule({
  declarations: [GratitudeWallComponent],
  imports: [
    CommonModule,
    GratitudeWallRoutingModule
  ]
})
export class GratitudeWallModule { }
