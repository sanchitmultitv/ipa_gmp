import { Component, OnInit } from '@angular/core';
import * as Clappr from 'clappr';

@Component({
  selector: 'app-gratitude-wall',
  templateUrl: './gratitude-wall.component.html',
  styleUrls: ['./gratitude-wall.component.scss']
})
export class GratitudeWallComponent implements OnInit {
  player:any;
  videoPlayer = 'assets/video/Mission_low.mp4';

  constructor() { }

  ngOnInit(): void {
    this.playVideo();
  }
  playVideo() {
    var playerElement = document.getElementById("player-wrapper");
    this.player = new Clappr.Player({
      parentId: 'player-wrapper',
      source: this.videoPlayer,
      poster: 'assets/ipf/images/mainstage.jpg',
      maxBufferLength: 30,
      width: '100%',
      height: '100vh',
      autoPlay: true,
      loop: true,
      hideMediaControl: true,
      hideVolumeBar: true,
      hideSeekBar: true,
      persistConfig: false,
      // chromeless: true,
      // mute: true,
      visibilityEnableIcon: false,
      disableErrorScreen: true,
      playback: {
        playInline: true,
        // recycleVideo: Clappr.Browser.isMobile,
        recycleVideo: true
      },
    });
    this.player.attachTo(playerElement);    
  }
}
