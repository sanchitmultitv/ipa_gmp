import { Router } from '@angular/router';
import { SteupServiceService } from 'src/app/services/steup-service.service';

import { Location } from '@angular/common';
import { Component, ElementRef, HostListener, OnInit, Renderer2, ViewChild } from '@angular/core';
import { FetchDataService } from 'src/app/services/fetch-data.service';
declare var $:any;

@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.scss']
})
export class LobbyComponent implements OnInit {
  @ViewChild('videoStream', { static: true }) videoElement: ElementRef;
  showImage = false;
  cameraStream;
  
  pages=[
    // {id:1, path:'/lounge', analytics: 'click_networkinglounge'},
    {id:2, path:'/eventhall', analytics: 'click_eventhall'},
    // {id:4, path:'/photobooth', analytics: 'click_photobooth'},
    {id:8, path:'/gratitudewall', analytics: 'click_gratitudewall'}
  ];
  popups=[
    {id:5, datatarget:'agendaModal', analytics: 'click_agenda'},
    {id:6, datatarget:'helpDeskModal', analytics: 'click_helpdesk'},
    {id:7, datatarget:'speakersModal', analytics: 'click_speakerprofile'},
  ];
  function=[
    {id:4, function:'showPhotobooth', analytics: 'click_photobooth'},
    // {id:7, datatarget:'speakersModal', analytics: 'click_speakerprofile'},
  ];
  speaker;
  constructor(private renderer: Renderer2, private _fd: FetchDataService, private location: Location,private router: Router, private _analytics: SteupServiceService) { }

  ngOnInit(): void {
    this.stepup('click_lobby');
    // let videoplay:any = document.getElementById('lobbyBgVideo');
    // videoplay.play();
    // let centerPlay:any = document.getElementById('centerPlay');
    // centerPlay.play();
    this.speaker = JSON.parse(localStorage.getItem('virtual')).category;
  }
  stepup(action){
    this._analytics.stepUpAnalytics(action);
  }

  showPhotobooth(){
    // this.newImage = value
    // $('.window').modal('hide');
    $('#photoboothModal2').modal('show');
    this.cameraStream = null;
    this.showImage = false;
    this.startWebcam();
  }
  startWebcam(){
    // const constraints = { "video": { width: 320, height: 180, facingMode: "user" }};
    let constraints = {
      facingMode: { exact: 'environment' },
      video: {
        width: { ideal: 640 },
        height: { ideal: 360 }
      }
    };
    navigator.mediaDevices.getUserMedia(constraints).then(this.gotMedia.bind(this)).catch(e=>{
        console.error('getusermedia() failed: '+e);
    })
  }
  theRecorder:any;
  gotMedia(stream){
    this.cameraStream = stream;
    this.renderer.setProperty(this.videoElement.nativeElement, 'srcObject', stream);
  }
  dataUrl;
  img;
  capturePhoto2(){
    this.showImage = true;
    let canvasRec:any = document.getElementById('canvas2');
    let preview:any = document.getElementById('videobgStream2');
    let context = canvasRec.getContext('2d');
    var cw = 640;
    var ch = 360;
    canvasRec.width = cw;
    canvasRec.height = ch;
    context.drawImage(preview, 0, 0, cw, ch ); 
       
    let watermark = new Image();
    let bgimg = new Image();
    context.beginPath();
    
    bgimg.src = 'assets/ipf/images/photobooth/selfie.png';
    context.drawImage(bgimg, 0, 0, preview.videoWidth, preview.videoHeight);
    this.dataUrl = canvasRec.toDataURL('mime');
    this.img = canvasRec.toDataURL("image/png", 0.7);
  }
  @HostListener('document:click', ['$event', '$event.target'])
    onClick(event: MouseEvent, targetElement: HTMLElement): void {
      let selfie:any=document.getElementById('photoboothModal2');
      if(targetElement===selfie){
        this.closePhotobooth();
      }
  }
  @HostListener('keydown', ['$event']) onKeyDown(e) {
    if (e.keyCode == 27) {
      this.closePhotobooth();
    }
  }
  closePhotobooth(){
    this.cameraStream.getTracks().forEach(track => { track.stop(); }); 
    $('#photoboothModal2').modal('hide');
    // this.location.back();
  }
  closeWindow(){
    $('.window').modal('hide');
  }
  downloadPic(){
    window.location.href=this.img;
    let user_id = JSON.parse(localStorage.getItem('virtual'));
    let user_name = JSON.parse(localStorage.getItem('virtual'));
    const event_id = '183';
    const formData = new FormData();
    formData.append('user_id', user_id.id);
    formData.append('user_name', user_id.name);
    formData.append('image', this.img);
    formData.append('event_id', event_id);
    this._fd.uploadsample(formData).subscribe(res => {
      console.log('upload', res);
    });
    
  }
  reload() {
    this.cameraStream.getTracks().forEach(track => { track.stop(); }); 
    this.showImage = false;
    this.startWebcam();
  }
}
