import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductsComponent } from './products.component';
import { ProductshomeComponent } from './productshome/productshome.component';
import { ProductslistComponent } from './productslist/productslist.component';
import { TestingComponent } from './testing/testing.component';


const routes: Routes = [
  {
    path:'',
    component: ProductsComponent,
    children:[
      {
        path:'', redirectTo:'home', pathMatch:'full'
      },
      {
        path:'home', component: ProductshomeComponent
      },
      {
        path:'list', component: ProductslistComponent
      },
      {
        path:'test', component: TestingComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }
