import { Directive, ElementRef, HostListener, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appBtn]'
})
export class BtnDirective {
  domElement:any;
  constructor(private renderer: Renderer2, private elementRef: ElementRef) { 
    this.setFontcolor();
  }
  setFontcolor(){
    this.domElement = this.elementRef.nativeElement;  // to get DOM element and store it in global variable
    // setting compulsory required styles to the DOM element
    const requiredStyles = {
      'background-color': 'yellow',
      'color': 'red',
      'font-weight': 'bold',
    };
    console.log('object', Object.keys(requiredStyles))
    Object.keys(requiredStyles).forEach(newStyle => {
      // this.domElement.style.setProperty(`${newStyle}`, elipsifyme[newStyle]);
      this.renderer.setStyle(
        this.domElement, `${newStyle}`, requiredStyles[newStyle]
      );
    });
  }
  @HostListener('click') onClick(){
    this.setFontcolor();
  }
}
