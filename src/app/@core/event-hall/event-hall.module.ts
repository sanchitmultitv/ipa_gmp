import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EventHallRoutingModule } from './event-hall-routing.module';
import { EventHallComponent } from './event-hall.component';
import { HallChatComponent } from './components/hall-chat/hall-chat.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { QnaComponent } from './components/qna/qna.component';


@NgModule({
  declarations: [
    EventHallComponent,
    HallChatComponent,
    QnaComponent],
  imports: [
    CommonModule,
    EventHallRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class EventHallModule { }
