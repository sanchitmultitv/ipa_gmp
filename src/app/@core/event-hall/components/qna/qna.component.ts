import { AfterViewInit, Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { ChatService } from 'src/app/services/chat.service';
import { FetchDataService } from 'src/app/services/fetch-data.service';
declare var $:any;
@Component({
  selector: 'app-qna',
  templateUrl: './qna.component.html',
  styleUrls: ['./qna.component.scss']
})
export class QnaComponent implements OnInit, OnDestroy {  
  textMessage = new FormControl('');
  messageList = [];
  user_name = JSON.parse(localStorage.getItem('virtual')).name;
  storage:any=JSON.parse(localStorage.getItem('virtual'));
  room_name = 'eventhall';
  alertMsg = 'Please Post Your Question';
  constructor(private router: Router, private chatService: ChatService, private _fd: FetchDataService) { }  
  ngOnInit(): void {
    $('#qnaModal').modal('show');
  }
  ngAfterViewInit() {
        
  }
  @HostListener('document:click', ['$event', '$event.target'])
  onClick(event: MouseEvent, targetElement: HTMLElement): void {
    let chat: any = document.getElementById('qnaModal');
    if(targetElement===chat){
      this.router.navigate(['/eventhall']);
    }
  }
  closeChat(){
    this.router.navigate(['/eventhall']);
  }
  
  postQuestion(value){
    let data = JSON.parse(localStorage.getItem('virtual'));
    let audi_id = '205060'
    this._fd.askLiveQuestions(data.id,value,audi_id).subscribe((res=>{
      if(res.code == 1){
        this.alertMsg = 'Submitted Succesfully';
        setTimeout(() => {
          this.router.navigate(['/eventhall']);
        }, 1000);
      }
      this.textMessage.reset(); 
    }));
  }
  ngOnDestroy(){
    
  }
}  
  
