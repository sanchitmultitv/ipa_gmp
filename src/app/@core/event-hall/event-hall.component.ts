import { Component, HostListener, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as Clappr from 'clappr';
import { FetchDataService } from 'src/app/services/fetch-data.service';
@Component({
  selector: 'app-event-hall',
  templateUrl: './event-hall.component.html',
  styleUrls: ['./event-hall.component.scss']
})
export class EventHallComponent implements OnInit {
  player:any;
  constructor(private router: Router, private _fd:FetchDataService) { }

  ngOnInit(): void {
    let event_id = 183;
    this._fd.getPlayerUrl(event_id).subscribe((res:any)=>{
      let stream = res.result[0]['stream'];
      let poster = res.result[0].poster;
      this.playVideo(stream,poster);
    });
  }
  playVideo(stream,poster) {
    var playerElement = document.getElementById("player-wrapper");
    this.player = new Clappr.Player({
      parentId: 'player-wrapper',
      source: stream,
      poster: poster,
      maxBufferLength: 30,
      width: '100%',
      // height: '500',
      // width: window.innerWidth*.5781,
      height: window.innerWidth*.5885/16*9,
      autoPlay: true,
      hideMediaControl: true,
      hideVolumeBar: true,
      hideSeekBar: true,
      persistConfig: false,
      // chromeless: true,
      // mute: true,
      visibilityEnableIcon: false,
      disableErrorScreen: true,
      playback: {
        playInline: true,
        // recycleVideo: Clappr.Browser.isMobile,
        recycleVideo: true
      },
    });
    this.player.attachTo(playerElement);    
  }
  gotoChat(){
    this.router.navigate(['/eventhall/chat']);
  }
  @HostListener('window:resize', ['$event']) onResize(event) {
    this.player.resize({ width: window.innerWidth*.5781, height: window.innerWidth*.5707/16*9 });
  }
}
