import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HallChatComponent } from './components/hall-chat/hall-chat.component';
import { QnaComponent } from './components/qna/qna.component';
import { EventHallComponent } from './event-hall.component';


const routes: Routes = [
  {
    path:'', 
    component: EventHallComponent,
    children:[
      {
        path:'chat', component: HallChatComponent
      },
      {
        path:'qna', component: QnaComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EventHallRoutingModule { }
