import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiConstants } from './apiConfig/api.constants';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  baseUrl = environment.baseUrl;
  signup = ApiConstants.signup;
  login = ApiConstants.login;
  constructor(private http: HttpClient) { }

  register(user: any){
    const event_id = 183;
    return this.http.post(`${this.baseUrl}/virtualapi/v1/auth/consume/attendees/event_id/${event_id}`, user);
  }
  // https://goapi.multitvsolution.com:7000/virtualapi/v1/auth/consume/attendees/event_id/183

  // loginSubmit(user:any){
  //   return this.http.get(`${this.baseUrl}/virtualapi/v1/asianpaint/attendees/login/event_id/${user.event_id}/name/${user.name}/email/${user.email}`)
  // }
  updateStatus(status){
    return this.http.post(`${this.baseUrl}/virtualapi/v1/attendee/status/update`, status);
  }
  successMail(success){
    return this.http.post(`https://goapi.multitvsolution.com:7000/virtualapi/v1/auth/sendemail/event_id/183`, success);
  }
  loginSubmit(email){
    return this.http.get(`  https://goapi.multitvsolution.com:7000/virtualapi/v1/auth/login/event_id/183/email/${email}/role_id/1`);
  }
  
  loginMethod(loginObj:any){
    return this.http.post(`https://goapi.multitvsolution.com:7000/virtualapi/v1/auth/auto/login/event_id/183`,loginObj);
  }
  logout(user_id):Observable<any>{
    return this.http.get(`${this.baseUrl}/${ApiConstants.logout}/user_id/${user_id}`);
  }
  acmeLoggedinMethod(loginObj:any):Observable<any>{
    return this.http.get(`${this.baseUrl}/${ApiConstants.acmeLogin}/event_id/${loginObj.event_id}/email/${loginObj.email}/registration_number/${loginObj.registration_number}`);
  }
}
